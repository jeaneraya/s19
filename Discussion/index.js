//alert("Hello");

/*
	Selection COntrol Structures
		- it sorts out whether the statement/s are to be executed based on the condition whether it is true or false
		- two way selection (true or false)
		- multi-way selection 

*/

// IF ELSE STATEMENTS 

/*
	Syntax:
		If(condition){
			statement
		}else if(condition){
			statement
		}else{
			statement
		}
*/

/*
	if statement - Executes a statement if a specified condition is true
*/

let numA = -1;

if(numA < 0){
	console.log("Hello");
}

console.log(numA < 0);

let city = "New York";

if (city === "New York"){
	console.log("Welcome to New York City");
}

/*
	When an If...Then...Else statement is encountered, condition is tested. If condition is True, the statements following Then are executed. If condition is False, each ElseIf statement (if there are any) is evaluated in order.
*/

/*
	Else if
		- executes a statement if our previous conditions are false and if the specified condition is true
		- the "else if" clause is optional and can be added to capture additional conditions to change the flow of a program
*/

let numB = 1;

if (numA > 0){
	console.log("Hello");
}else if (numB > 0){
	console.log("World");
}

city = "Tokyo";

if (city === "New York"){
	console.log("Welcome to New York City");
}else if (city === "Tokyo"){
	console.log("Welcome to Tokyo City");
}

/*
	else statement
		- executes a statement if all of the condition is false
*/

if (numA > 0){
	console.log("Hello");
}else if (numB === 0){
	console.log("World");
}else {
	console.log("Again");
}

/*let age = parseInt(prompt("Enter your age:"));

if (age <= 18) {
	console.log("Not allowed to drink");
} else {
	console.log("Congrats! You can now take a shot");
}*/

function getHeight(height){
	if (height <= 150) {
	console.log("Did not pass the minimum height requirement");
} else if (height >= 150) {
	console.log("Passed the minimum height requirement");
}
}

/*let heightEntered = parseInt(prompt("Enter height:"));
getHeight(heightEntered);

// Nested if statement

let isLegalAge = true;
let isAdmin = false;

if (isLegalAge) {
	if(!isAdmin) {
		console.log("You are not an admin");
	}
};*/

let message = "No message";

function determineTyphoonIntensity(windSpeed){
	if (windSpeed < 30){
		return "Not a Typhoon";
	}else if (windSpeed <=61) {
		return "Tropical Depression Detected.";
	}else if (windSpeed >= 62 && windSpeed <= 88) {
		return "Tropical storm Detected.";
	}else if (windSpeed >= 89 && windSpeed <= 177){
		return "Severe Tropical storm Detected";
	}else{
		return "Typhoon Detected";
	}
};

message = determineTyphoonIntensity(70);
console.log(message);

// Truthy and Falsy

/*
	In JavaScript truthy value is a value that is considered true when encountered in a boolean context;

	Falsy values:
		1. false
		2. 0
		3. -0
		4. ""
		5. null
		6. undefined
		7. NaN
*/

// Truthy examples

let word = "true";

if (word) {
	console.log("Truthy");
}

if (true){
	console.log("Truthy");
}

if(1) {
	console.log("Truthy");
}

if ([]){
	console.log("Truthy");
}

//Falsy examples;

if(false){
	console.log("Falsy");
}

if(0){
	console.log("Falsy");
}
if(-0){
	console.log("Falsy");
}

if(undefined){
	console.log("Falsy");
}

if(null){
	console.log("Falsy");
}

if(NaN){
	console.log("Falsy");
}

//Conditional (Ternary) Operator - for short codes

/*
	Ternary Operator takes in three operands
		1. 
*/

//Single Statement Execution

/*let ternaryResult = (1 < 18) ? "Condition is True" : "Condition is False"

console.log("Result of Ternary Operator:" + ternaryResult);

let name;

function isOfLegalAge(){
	name = "John";
	return "You are of the legal age limit";
}

function isUnderAge(){
	name = "Jean";
	return "You are under the age limit";
}

let yourAge = parseInt(prompt("What is your age?"));

let legalAge = (yourAge > 18) ? isOfLegalAge() :isUnderAge();

console.log("Result of Ternary Operator in Function: " + legalAge + ", " + name);*/


/*
	Syntax
*/

/*let day = prompt("What day of the week is it today?").toLowerCase()

console.log(day);

switch(day) {
	case "monday":
		console.log("The color of the day is red.");
		break;
	case "tuesday":
		console.log("The color of the day is purple.");
		break;
	case "wednesday":
		console.log("The color of the day is orange.");
		break;
	case "thursday":
		console.log("The color of the day is blue.");
		break;
	case "friday":
		console.log("The color of the day is yellow.");
		break;
	case "saturday":
		console.log("The color of the day is indigo.");
		break;
	case "sunday":
		console.log("The color of the day is green.");
		break;	
	default:
		console.log("Please input a valid day");
		breakl
}*/

// Try-Catch-Finally Statement

/*
	- try-catch is commonly used for error handling
	- will still function even if the statement is not complete
*/

function showIntensityAlert(windSpeed){
	try {
		alert(determineTyphoonIntensity(windSpeed))
	}
	catch (error){
		//typeoff - check what type is our variable
		console.log(typeof error)
		console.log(error)
		console.log(error.message)
	}
	finally {
		alert("Intensity updates will show new alert!")
	}
}

showIntensityAlert(76);
